require('dotenv').config();
const jwt = require('jsonwebtoken');
const { UserGame } = require('../models');
const controller = {};

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

controller.signup = async (req, res, next) => {

    try {
        const newUser = await UserGame.create({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email
        });

        const token = signToken(newUser.user_game_id);

        res.status(201).json({
            status: 'success',
            token,
            data: {
                user: newUser
            }
        })
    } catch (error) {
        res.status(404).json({
            status: 'fail',
            message: error
        })
    }
}

controller.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            res.status(400).json({
                status: "error",
                message: "Please provide email and password!"
            })
        }

        const user = await UserGame.findOne({ where: { email: email, password: password } });

        if (!user) {
            res.status(401).json({
                status: "error",
                message: "Incorrect email or password"
            })
        }

        const token = signToken(user.user_game_id);

        res.status(200).json({
            status: 'success',
            token
        })
    } catch (error) {
        res.status(404).json({
            status: 'fail',
            message: error
        })
    }
}

controller.protect = async (req, res, next) => {
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1];
    }

    if (!token) {

        return next(
            res.status(401).json({
                status: "error",
                message: "You are not logged in! Please log in to get access."
            })
        );
    }

    next()
}

module.exports = controller;