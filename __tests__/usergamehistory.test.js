require('dotenv').config();
const jwt = require('jsonwebtoken');
const app = require('../app');
const { sequelize, UserGame } = require('../models');


const request = require('supertest');

const usergamehistory = require('../masterdata/usergamehistory.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
})

const userPayload = {
    username: "gembox",
    password: 'gem123',
    email: 'ggembox@mail.com'
};

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

beforeAll((done) => {
    sequelize.queryInterface
        .bulkInsert('UserGameHistories', usergamehistory, {})
        .then(() => {
            done()
        })
        .catch((err) => {
            done()
        })
})

afterAll((done) => {
    sequelize.queryInterface
        .bulkDelete('UserGameHistories', null, { truncate: true, restartIdentity: true })
        .then(() => {
            done()
        })
        .catch((err) => {
            done()
        })
})


describe("given the user is not logged in", () => {
    it("should return a 401", async () => {
        const { statusCode } = await request(app).post("/api/v1/usergamehistory");

        expect(statusCode).toBe(401);
    });
});

describe("given the user is logged in", () => {
    describe("create usergamehistory route", () => {
        it("should return a 201 and create the usergamehistory", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamehistorypayload = {
                skor: 100,
                user_game_id: 1
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamehistory")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamehistorypayload);

            expect(statusCode).toBe(201);
        });

        it("should return a 400 and error create the usergamehistory", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamehistorypayload = {
                skor: "gemma",
                user_game_id: "yuhuu"
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamehistory")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamehistorypayload);

            expect(statusCode).toBe(400);
        });

        it("should return a 400", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamehistorypayload = {
                skor: 100,
                user_game_id: "yuhuu"
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergamehistory")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamehistorypayload);

            expect(statusCode).toBe(400);
        });
    });

    describe("get all usergamehistory route", () => {
        it("should return a 200 and the usergamehistory", async () => {

            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const { statusCode, body } = await request(app)
                .get("/api/v1/usergamehistory")
                .set("Authorization", `Bearer ${jwt}`)

            expect(statusCode).toBe(200);

        });
    })

    describe("get by id usergamehistory route", () => {
        describe("given the usergamehistory does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "1234";
                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergamehistory/${userGameHistoryId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergamehistory does exists", () => {
            it("should return a 200 status and the usergamehistory", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "1"

                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergamehistory/${userGameHistoryId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(200);

            });
        });
    });

    describe("update usergamehistory route", () => {
        describe("given the usergamehistory does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "1234";
                const { body, statusCode } = await request(app).put(
                    `/api/v1/usergamehistory/${userGameHistoryId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });


        });

        describe("given the usergamehistory does exists", () => {
            it("should return a 201 and update the usergamehistory", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "1";

                const usergamehistorypayload = {
                    skor: 80,
                    user_game_id: 1
                }

                const { statusCode, body } = await request(app)
                    .put(`/api/v1/usergamehistory/${userGameHistoryId}`)
                    .set("Authorization", `Bearer ${jwt}`)
                    .send(usergamehistorypayload);

                expect(statusCode).toBe(201);
            });
        })


    });

    describe("delete usergamehistory route", () => {
        describe("given the usergamehistory does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "1234";
                const { body, statusCode } = await request(app).delete(
                    `/api/v1/usergamehistory/${userGameHistoryId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergamehistory does exists", () => {
            it("should return a 201 and delete the usergamehistory", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameHistoryId = "3";

                const { statusCode, body } = await request(app)
                    .delete(`/api/v1/usergamehistory/${userGameHistoryId}`)
                    .set("Authorization", `Bearer ${jwt}`)

                expect(statusCode).toBe(201);
            });
        })


    });

})
