require('dotenv').config();
const jwt = require('jsonwebtoken');
const app = require('../app');
const { UserGame } = require('../models');

const request = require('supertest');

const userPayload = {
    username: "gembox",
    password: 'gem123',
    email: 'ggembox@mail.com'
};

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

describe("given the user is not logged in", () => {
    it("should return a 401", async () => {
        const { statusCode } = await request(app).post("/api/v1/usergame");

        expect(statusCode).toBe(401);
    });
});

describe("given the user is logged in", () => {
    describe("create usergame route", () => {
        it("should return a 201 and create the usergame", async () => {
            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const usergamepayload = {
                username: "tamu",
                email: "tamu@mail.com",
                password: "12345678"
            }

            const { statusCode, body } = await request(app)
                .post("/api/v1/usergame")
                .set("Authorization", `Bearer ${jwt}`)
                .send(usergamepayload);

            expect(statusCode).toBe(201);
        });
    });

    describe("get all usergame route", () => {
        it("should return a 200 and the usergame", async () => {

            const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

            const jwt = signToken(user.user_game_id);

            const { statusCode, body } = await request(app)
                .get("/api/v1/usergame")
                .set("Authorization", `Bearer ${jwt}`)

            expect(statusCode).toBe(200);

        });
    })

    describe("get by id usergame route", () => {
        describe("given the usergame does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = "1234";
                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergame/${userGameId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergame does exists", () => {
            it("should return a 200 status and the usergame", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = "1"

                const { body, statusCode } = await request(app).get(
                    `/api/v1/usergame/${userGameId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(200);

            });
        });
    });

    describe("update usergame route", () => {
        describe("given the usergame does not exists", () => {
            it("should return a 400", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = "1234";
                const { body, statusCode } = await request(app).put(
                    `/api/v1/usergame/${userGameId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(400);
            });


        });

        describe("given the usergame does exists", () => {
            it("should return a 201 and update the usergame", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = 1;

                const usergamepayload = {
                    username: "hayuk",
                    email: "hayuk@mail.com",
                    password: "123456789"
                }

                const { statusCode, body } = await request(app)
                    .put(`/api/v1/usergame/${userGameId}`)
                    .set("Authorization", `Bearer ${jwt}`)
                    .send(usergamepayload);

                expect(statusCode).toBe(201);
            });
        })


    });

    describe("delete usergame route", () => {
        describe("given the usergame does not exists", () => {
            it("should return a 404", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = "1234";
                const { body, statusCode } = await request(app).delete(
                    `/api/v1/usergame/${userGameId}`
                ).set("Authorization", `Bearer ${jwt}`);

                expect(statusCode).toBe(404);
            });
        });

        describe("given the usergame does exists", () => {
            it("should return a 201 and delete the usergame", async () => {
                const user = await UserGame.findOne({ where: { email: userPayload.email, password: userPayload.password } });

                const jwt = signToken(user.user_game_id);

                const userGameId = 2;

                const { statusCode, body } = await request(app)
                    .delete(`/api/v1/usergame/${userGameId}`)
                    .set("Authorization", `Bearer ${jwt}`)

                expect(statusCode).toBe(201);
            });
        })


    });

})
